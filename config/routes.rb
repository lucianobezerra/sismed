Rails.application.routes.draw do
  root to: "home#index"

  resources :pacientes do
    resources :agendamentos
  end
  
  resources :medicos do
    resources :agendamentos
  end

  resources :agendamentos do
    match '/medicos', to: "agendamentos#medicos", via: "post"
  end

  match '/by_interval', to: "agendamentos#by_interval", via: "post"
end
