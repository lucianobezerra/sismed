class CreateAgendamentos < ActiveRecord::Migration
  def change
    create_table :agendamentos do |t|
      t.integer :paciente_id
      t.integer :medico_id
      t.timestamp :agenda

      t.timestamps null: false
    end
  end
end
