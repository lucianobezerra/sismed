FactoryGirl.define do
  factory :agendamento do
    medico_id  1
    paciente_id 1
    agenda Date.today
  end
end