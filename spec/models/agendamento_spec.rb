require 'rails_helper'

describe Agendamento, :type => :model do
  context "Attributes" do
    let(:ag){build(:agendamento)}

    it{expect(ag).to be_valid}
    it{expect(ag).to respond_to(:medico_id, :paciente_id, :agenda)}    
  end

  context "Common Validates" do
    let(:ag){Agendamento.new}
    before{ag.valid?}

    it "should be require a medical" do
      expect(ag.errors.messages).to include(:medico_id)
    end

    it "should be require a patient" do
      expect(ag.errors.messages).to include(:paciente_id)
    end

    it "should be require a data" do
      expect(ag.errors.messages).to include(:agenda)
    end
  end

  context "Associations" do
    it "should have one medical" do
      expect(Agendamento.reflect_on_association(:medico).macro).to eq :belongs_to
    end

    it "should have one patient" do
      expect(Agendamento.reflect_on_association(:paciente).macro).to eq :belongs_to
    end
  end

end