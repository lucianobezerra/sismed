class MedicosController < ApplicationController
  def index
    @medicos = Medico.all
  end

  def new
    @medico = Medico.new
  end

  def create
    @medico = Medico.new(medico_params)
    if @medico.save
      flash[:success] = "Medico Cadastrado"
      redirect_to medicos_path
    else
      flash[:danger] = "Verifique o Formulário"
      render :new
    end    
  end

  def edit
    @medico = Medico.find(params[:id])
  end

  def update
    @medico = Medico.find(params[:id])
    if @medico.update_attributes(medico_params)
      flash[:success] = "Médico Atualizado"
      redirect_to medicos_path
    else
      flash[:danger] = "Erro ao Atualizar"
      render 'edit'
    end    
  end

  def destroy
    @medico = Medico.find(params[:id])
    if @medico.destroy
      flash[:success] = "Médico Excluido"
      redirect_to medicos_path
    end
  end

  private
  def medico_params
    params.require(:medico).permit(:nome, :crm)
  end
end