class AgendamentosController < ApplicationController
  respond_to :json, :html, :js

  def index
    @agendamento = Agendamento.new
    @agendamentos = Agendamento.all
      if params[:medico_id]
      @agendamentos = Agendamento.by_doctor(params[:medico_id])
    end

    if !params[:data_inicial].blank?
      @agendamentos = Agendamento.by_data(params[:data_inicial], params[:data_final])
    end
    respond_to do |format|
      format.html
      format.json { render json: @agendamentos.to_json(include: [:medico, :paciente]) }
    end

  end

  def new
    @agendamento = Agendamento.new
  end

  def create
    @agendamento = Agendamento.new(agendamento_params)
    respond_to do |format|
      if @agendamento.save
        format.html {redirect_to agendamentos_path, success: "Agendamento Gravado"}
      else
        format.html {render new, danger: "Verifique o Formulario"}
      end
    end      
  end

  def destroy
    @agendamento = Agendamento.find(params[:id])
    @id_med = @agendamento.medico_id
    respond_to do |format|
      if @agendamento.destroy
        format.js {}
      end      
    end
  end

  private
  def agendamento_params
    params.require(:agendamento).permit(:medico_id, :paciente_id, :agenda)
  end
 
end