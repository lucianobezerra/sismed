class PacientesController < ApplicationController
	def index
		@pacientes = Paciente.all
	end

  def new
    @paciente = Paciente.new
  end

  def create
    @paciente = Paciente.new(paciente_params)
    if @paciente.save
      flash[:success] = "Paciente Cadastrado"
      redirect_to pacientes_path
    else
      flash[:danger] = "Verifique o Formulário"
      render :new
    end    
  end

  def edit
    @paciente = Paciente.find(params[:id])
  end

  def update
    @paciente = Paciente.find(params[:id])
    if @paciente.update_attributes(paciente_params)
      flash[:success] = "Paciente Atualizado"
      redirect_to pacientes_path
    else
      flash[:danger] = "Erro ao Atualizar"
      render 'edit'
    end
  end

  def destroy
    @paciente = Paciente.find(params[:id])
    if @paciente.destroy
      flash[:success] = "Paciente Excluido"
      redirect_to pacientes_path
    end
  end

  private
  def paciente_params
    params.require(:paciente).permit(:nome, :cpf)
  end
end