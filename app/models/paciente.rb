class Paciente < ActiveRecord::Base
  validates :nome, presence: {message: "Nome é Obrigatório"}
  validates :cpf,  presence: {message: "CPF é Obrigatório" }
  validates :cpf, :uniqueness => true

  has_many :agendamentos
  def to_s
    self.nome
  end
end
