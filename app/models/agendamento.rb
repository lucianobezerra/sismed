  class Agendamento < ActiveRecord::Base
  belongs_to :medico
  belongs_to :paciente

  validates :medico_id, :presence => true
  validates :paciente_id, :presence => true
  validates :agenda, :presence => true
  validates :agenda, :uniqueness => {:scope => [:medico_id, :paciente_id]}
  
  def self.by_doctor(search)
    where("medico_id = ?", search) 
  end

  def self.by_data(data_inicial, data_final)
    initial = data_inicial.split('/')
    final   = data_final.split('/')
    d1 = initial[2] + '-' + initial[1] + '-' + initial[0]
    d2 = final[2] + '-' + final[1] + '-' + final[0]
    where("agenda::date between ? and ?", "#{d1}", "#{d2}") 
  end
end
