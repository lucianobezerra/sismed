class Medico < ActiveRecord::Base
  validates :nome, presence: {message: "Nome é Obrigatório"}
  validates :crm,  presence: {message: "CRM é Obrigatório" }
  validates :crm, format: { with: /\A([A-Z]{2}+)([0-9]{6})\z/, message: "Verifique o Formato do CRM" }

  has_many :agendamentos
  def to_s
    self.nome
  end
end