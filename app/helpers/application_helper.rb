module ApplicationHelper
  def formata_cpf(value)
    return "#{value[0..2]}.#{value[3..5]}.#{value[6..8]}-#{value[9..10]}"
  end

  def flash_message
    messages = ""
    [:success, :info, :warning, :danger].each {|type|
      if flash[type]
        messages += "<div class=\"alert alert-#{type} alert-dismissable\">"
        messages += "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
        messages += "<strong>#{flash[type]}</strong>"
        messages += "</div>"
      end
    }
    messages
  end
  
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
  end

end
